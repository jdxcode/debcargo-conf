From 757ae135ee5a877b7baa948fc121336b96f96fab Mon Sep 17 00:00:00 2001
From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Thu, 24 Oct 2024 19:35:28 -0400
Subject: [PATCH] Feature-gate the use of X448 on the unstable-curve448 feature of pgp

See https://github.com/rpgp/rpgp/pull/426 -- this probably should be
blocked on the conclusion there.

This is aligned with Debian's existing patches on rpgp for now.

Forwarded: https://codeberg.org/heiko/rsop/pulls/18
---
 rpgpie-sop/Cargo.toml          | 3 +++
 rpgpie-sop/src/cmd/generate.rs | 4 ++++
 2 files changed, 7 insertions(+)

diff --git a/rpgpie-sop/src/cmd/generate.rs b/rpgpie-sop/src/cmd/generate.rs
index 33a5271..065713b 100644
--- rpgpie-sop/src/cmd/generate.rs
+++ rpgpie-sop/src/cmd/generate.rs
@@ -18,6 +18,7 @@ const PROFILE_NISTP521: &str = "interop-testing-rfc6637-nistp521";
 
 const PROFILE_RFC9580_NISTP: &str = "interop-testing-rfc9580-nistp";
 const PROFILE_RFC9580_RSA: &str = "interop-testing-rfc9580-rsa";
+#[cfg(feature = "unstable-curve448")]
 const PROFILE_RFC9580_CV448: &str = "interop-testing-rfc9580-cv448";
 
 const PROFILES: &[(&str, &str)] = &[
@@ -34,6 +35,7 @@ const PROFILES: &[(&str, &str)] = &[
         PROFILE_RFC9580_NISTP,
         "Only for interop-testing: use algorithms from RFC 9580 with NIST P-256",
     ),
+    #[cfg(feature = "unstable-curve448")]
     (
         PROFILE_RFC9580_CV448,
         "Only for interop-testing: use algorithms from RFC 9580 with X448 and Ed25519",
@@ -79,6 +81,7 @@ impl<'a> sop::ops::GenerateKey<'a, RPGSOP, Keys> for GenerateKey {
             PROFILE_RFC9580 => PROFILE_RFC9580,
             PROFILE_RFC9580_NISTP => PROFILE_RFC9580_NISTP,
             PROFILE_RFC9580_RSA => PROFILE_RFC9580_RSA,
+            #[cfg(feature = "unstable-curve448")]
             PROFILE_RFC9580_CV448 => PROFILE_RFC9580_CV448,
             _ => return Err(sop::errors::Error::UnsupportedProfile),
         };
@@ -190,6 +193,7 @@ impl<'a> sop::ops::GenerateKey<'a, RPGSOP, Keys> for GenerateKey {
                 });
             }
 
+            #[cfg(feature = "unstable-curve448")]
             PROFILE_RFC9580_CV448 => {
                 let tsk = Tsk::generate_v6(
                     pgp::KeyType::Ed25519, // FIXME: use Ed448 when rpgp supports it
-- 
2.45.2

