rust-ognibuild (0.0.32-2) unstable; urgency=medium

  * Team upload.
  * Package ognibuild 0.0.32 from crates.io using debcargo 2.7.5
  * Reduce tests on riscv64 to avoid timeout.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 21 Dec 2024 15:17:11 +0000

rust-ognibuild (0.0.32-1) unstable; urgency=medium

  * Package ognibuild 0.0.32 from crates.io using debcargo 2.7.5
  * Rearrange patches for easier testing.
  * Disable oversensitive test.
  * Make "upstream" feature depend on tokio.

  [ NoisyCoil ]
  * Team upload.
  * Package ognibuild 0.0.32 from crates.io using debcargo 2.7.5
  * d/patches:
    - add serialize-tests (fixes race condition due to parallel usage of
      breezyshim::testing::TestEnv in tests)
    - add relax-lazy-regex (bump lazy-regex to v3)
    - delete unused relax-buildlog-consultant

  [ Jelmer Vernooĳ ]
  * Package ognibuild 0.0.32 from crates.io using debcargo 2.7.5

 -- Peter Michael Green <plugwash@debian.org>  Sun, 15 Dec 2024 18:39:30 +0000

rust-ognibuild (0.0.31-1) unstable; urgency=medium

  * Package ognibuild 0.0.31 from crates.io using debcargo 2.7.2

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 06 Nov 2024 03:01:32 +0000

rust-ognibuild (0.0.30-1) unstable; urgency=medium

  * Package ognibuild 0.0.30 from crates.io using debcargo 2.7.2

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 02 Nov 2024 19:30:21 +0000

rust-ognibuild (0.0.28-1) unstable; urgency=medium

  * Move package to rust-team, change source package name to rust-ognibuild.
  * Package ognibuild 0.0.28 from crates.io using debcargo 2.7.0
    + Fixes compatibility with pyo3 0.22. Closes: ##1081087

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 12 Oct 2024 18:51:41 +0100

ognibuild (0.0.20-1) unstable; urgency=low

  * New upstream release.
  * Disable tests. Closes: #1071804

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 07 Jun 2024 14:43:54 +0100

ognibuild (0.0.18+git20230208.1.9b890a2-2) UNRELEASED; urgency=medium

  * Fix build source after successful build. Closes: #1047139

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 06 Sep 2023 20:18:06 +0100

ognibuild (0.0.18+git20230208.1.9b890a2-1) unstable; urgency=low

  * New upstream snapshot.
   + Fixes flaky tee test. Closes: #1030879

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Feb 2023 13:23:14 +0000

ognibuild (0.0.18-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 03 Feb 2023 02:12:19 +0000

ognibuild (0.0.16-2) unstable; urgency=medium

  * Add missing recommends on python3-build.
  * Update standards version to 4.6.2, no changes needed.
  * Fix autopkgtest running tests. Closes: #1027397

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 04 Jan 2023 19:58:27 +0000

ognibuild (0.0.16-1) unstable; urgency=low

  [ Jelmer Vernooĳ ]
  * New upstream release.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 16 Dec 2022 11:59:58 +0000

ognibuild (0.0.13-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper from old 12 to 13.
  * Re-export upstream signing key without extra signatures.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 14 Apr 2022 19:55:46 +0100

ognibuild (0.0.12-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 29 Mar 2022 18:06:58 +0100

ognibuild (0.0.11-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 24 Feb 2022 00:54:52 +0000

ognibuild (0.0.10-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 26 Dec 2021 15:58:53 +0000

ognibuild (0.0.9-1) unstable; urgency=low

  * New upstream release.
   + Improve maven requirements output. Closes: #989542
   + Print upstream commands run. Closes: #989454

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 12 Jun 2021 14:40:08 +0100

ognibuild (0.0.8-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 11 Jun 2021 20:21:58 +0100

ognibuild (0.0.6+git20210517.1.8189e91-1) unstable; urgency=low

  * New upstream snapshot.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 18 May 2021 20:53:15 +0100

ognibuild (0.0.5-1) unstable; urgency=low

  * New upstream release.
   + Fixes cmake support. Closes: #988572
   + Preserve environment when building Python packages. Closes: #988571

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 18 May 2021 01:34:11 +0100

ognibuild (0.0.4-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 07 Apr 2021 00:11:09 +0100

ognibuild (0.0.3-1) unstable; urgency=medium

  * Add missing dependency on python3-lz4.
  * Set upstream metadata fields: Security-Contact.
  * Add upstream signing keys.
  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 27 Mar 2021 17:49:29 +0000

ognibuild (0.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 02 Mar 2021 18:25:45 +0000

ognibuild (0.0.1~git20210228.bc79314-1) unstable; urgency=medium

  * New upstream snapshot.
  * Add dependency on buildlog-consultant.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 28 Feb 2021 14:55:03 +0000

ognibuild (0.0.1~git20201031.4cbc8df-1) unstable; urgency=low

  * Initial release. Closes: #981913

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 05 Feb 2021 03:00:40 +0000
