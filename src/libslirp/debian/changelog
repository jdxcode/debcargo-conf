rust-libslirp (4.3.2-1) unstable; urgency=medium

  * Package libslirp 4.3.2 from crates.io using debcargo 2.7.0
  * Drop nix-0.27.patch, no longer needed
  * Update relax-deps.patch for new upstream and current situation in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 23 Oct 2024 01:09:58 +0000

rust-libslirp (4.3.1-2) unstable; urgency=medium

  * Team upload.
  * Package libslirp 4.3.1 from crates.io using debcargo 2.6.1
  * Enable nix 0.27 patch.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 02 May 2024 15:23:42 +0000

rust-libslirp (4.3.1-1) unstable; urgency=medium

  * Team upload.
  * Package libslirp 4.3.1 from crates.io using debcargo 2.6.1
  * Update patches for new upstream.
  * Tighten nix dependency, unfortunately nix 0.27 requires changes that break
    builds with nix 0.26.
  * Add patch for nix 0.27 but don't enable it yet.
  * Bump etherparse dependency to 0.13
  * Mark tests for the "structopt" feature as broken.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 28 Feb 2024 09:53:14 +0000

rust-libslirp (4.3.0-4) unstable; urgency=medium

  * Team upload.
  * Package libslirp 4.3.0 from crates.io using debcargo 2.6.0
  * Bump enumflags2 dependency to 0.7.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 26 Jun 2023 23:24:47 +0000

rust-libslirp (4.3.0-3) unstable; urgency=medium

  * Team upload.
  * Package libslirp 4.3.0 from crates.io using debcargo 2.4.4
  * Don't relax dependency on mio, it seems the newer version is actually
    needed ( Closes: 981840 )
  * Set collapse_features = true, it seems that the features were already
    collapsed in the package that was uploaded, perhaps this was done
    manually.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 06 Feb 2021 18:11:59 +0000

rust-libslirp (4.3.0-2) unstable; urgency=high

  * Rebuild.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 10 Jan 2021 10:23:40 +0100

rust-libslirp (4.3.0-1) unstable; urgency=medium

  * Package libslirp 4.3.0 from crates.io using debcargo 2.4.3

  [ Christopher Obbard ]
  * Team upload.
  * Package libslirp 4.3.0 from crates.io using debcargo 2.4.3

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 23 Dec 2020 13:10:43 +0100
