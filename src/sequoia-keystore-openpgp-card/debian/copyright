Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sequoia-keystore-openpgp-card
Upstream-Contact: Neal H. Walfield <neal@sequoia-pgp.org>
Source: https://gitlab.com/sequoia-pgp/sequoia-keystore

Files: *
Copyright: 2024 Neal H. Walfield <neal@sequoia-pgp.org>
License: LGPL-2.0-or-later

Files:
 src/privkey.rs
 src/lib.rs
Copyright: 2021-2022 Heiko Schaefer <heiko@schaefer.name>
License: MIT OR Apache-2.0

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Alexander Kjäll <alexander.kjall@gmail.com>
 2024 Holger Levsen <holger@debian.org>
License: LGPL-2.0-or-later

License: LGPL-2.0-or-later
 Debian systems provide the LGPL 2.0 in /usr/share/common-licenses/LGPL-2

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
