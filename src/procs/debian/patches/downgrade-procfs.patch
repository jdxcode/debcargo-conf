From 1548ab68c839b604d89ede58b968f1c068af00ef Mon Sep 17 00:00:00 2001
From: NoisyCoil <noisycoil@tutanota.com>
Date: Mon, 30 Sep 2024 22:19:13 +0200
Subject: [PATCH] Downgrade procfs to v0.14

---
 Cargo.toml                  | 2 +-
 src/columns/cpu_time.rs     | 2 +-
 src/columns/elapsed_time.rs | 2 +-
 src/columns/start_time.rs   | 2 +-
 src/columns/usage_cpu.rs    | 2 +-
 src/columns/usage_mem.rs    | 6 +++---
 src/columns/vm_rss.rs       | 3 +--
 src/process/linux.rs        | 6 +++---
 8 files changed, 12 insertions(+), 13 deletions(-)

--- a/Cargo.toml
+++ b/Cargo.toml
@@ -159,7 +159,7 @@
 version = "0.16.1"
 
 [target.'cfg(any(target_os = "linux", target_os = "android"))'.dependencies.procfs]
-version = "0.17.0"
+version = "0.14"
 
 [target.'cfg(any(target_os = "linux", target_os = "android"))'.dependencies.regex]
 version = "1.11"
--- a/src/columns/cpu_time.rs
+++ b/src/columns/cpu_time.rs
@@ -29,7 +29,7 @@
 impl Column for CpuTime {
     fn add(&mut self, proc: &ProcessInfo) {
         let time_sec = (proc.curr_proc.stat().utime + proc.curr_proc.stat().stime)
-            / procfs::ticks_per_second();
+            / procfs::ticks_per_second().unwrap_or(100);
 
         let fmt_content = util::parse_time(time_sec);
         let raw_content = time_sec;
--- a/src/columns/elapsed_time.rs
+++ b/src/columns/elapsed_time.rs
@@ -11,7 +11,7 @@
 use std::collections::HashMap;
 
 #[cfg(any(target_os = "linux", target_os = "android"))]
-static TICKS_PER_SECOND: Lazy<u64> = Lazy::new(procfs::ticks_per_second);
+static TICKS_PER_SECOND: Lazy<u64> = Lazy::new(|| procfs::ticks_per_second().unwrap());
 
 pub struct ElapsedTime {
     header: String,
--- a/src/columns/start_time.rs
+++ b/src/columns/start_time.rs
@@ -11,7 +11,7 @@
 use std::collections::HashMap;
 
 #[cfg(any(target_os = "linux", target_os = "android"))]
-static TICKS_PER_SECOND: Lazy<u64> = Lazy::new(procfs::ticks_per_second);
+static TICKS_PER_SECOND: Lazy<u64> = Lazy::new(|| procfs::ticks_per_second().unwrap());
 
 pub struct StartTime {
     header: String,
--- a/src/columns/usage_cpu.rs
+++ b/src/columns/usage_cpu.rs
@@ -33,7 +33,7 @@
 
         let curr_time = curr_stat.utime + curr_stat.stime;
         let prev_time = prev_stat.utime + prev_stat.stime;
-        let usage_ms = (curr_time - prev_time) * 1000 / procfs::ticks_per_second();
+        let usage_ms = (curr_time - prev_time) * 1000 / procfs::ticks_per_second().unwrap_or(100);
         let interval_ms = proc.interval.as_secs() * 1000 + u64::from(proc.interval.subsec_millis());
         let usage = usage_ms as f64 * 100.0 / interval_ms as f64;
 
--- a/src/columns/usage_mem.rs
+++ b/src/columns/usage_mem.rs
@@ -1,7 +1,7 @@
 use crate::process::ProcessInfo;
 use crate::{column_default, Column};
 #[cfg(any(target_os = "linux", target_os = "android"))]
-use procfs::{Current, Meminfo, WithCurrentSystemInfo};
+use procfs::Meminfo;
 use std::cmp;
 use std::collections::HashMap;
 #[cfg(target_os = "windows")]
@@ -36,7 +36,7 @@
 
 #[cfg(any(target_os = "linux", target_os = "android"))]
 fn get_mem_total() -> u64 {
-    let meminfo = Meminfo::current();
+    let meminfo = Meminfo::new();
     if let Ok(meminfo) = meminfo {
         meminfo.mem_total
     } else {
@@ -97,7 +97,7 @@
 #[cfg(any(target_os = "linux", target_os = "android"))]
 impl Column for UsageMem {
     fn add(&mut self, proc: &ProcessInfo) {
-        let usage = proc.curr_proc.stat().rss_bytes().get() as f64 * 100.0 / self.mem_total as f64;
+        let usage = proc.curr_proc.stat().rss_bytes().unwrap_or(0) as f64 * 100.0 / self.mem_total as f64;
         let fmt_content = format!("{usage:.1}");
         let raw_content = (usage * 1000.0) as u32;
 
--- a/src/columns/vm_rss.rs
+++ b/src/columns/vm_rss.rs
@@ -29,8 +29,7 @@
 #[cfg(any(target_os = "linux", target_os = "android"))]
 impl Column for VmRss {
     fn add(&mut self, proc: &ProcessInfo) {
-        use procfs::WithCurrentSystemInfo;
-        let raw_content = proc.curr_proc.stat().rss_bytes().get();
+        let raw_content = proc.curr_proc.stat().rss_bytes().unwrap_or(0);
         let fmt_content = bytify(raw_content);
 
         self.fmt_contents.insert(proc.pid, fmt_content);
--- a/src/process/linux.rs
+++ b/src/process/linux.rs
@@ -1,6 +1,6 @@
 use procfs::process::{FDInfo, Io, Process, Stat, Status, TasksIter};
 use procfs::ProcError;
-use procfs::ProcessCGroup;
+use procfs::ProcessCgroup;
 use std::collections::HashMap;
 use std::path::PathBuf;
 use std::thread;
@@ -33,9 +33,9 @@
         }
     }
 
-    pub fn cgroups(&self) -> Result<Vec<ProcessCGroup>, ProcError> {
+    pub fn cgroups(&self) -> Result<Vec<ProcessCgroup>, ProcError> {
         match self {
-            ProcessTask::Process { proc: x, .. } => x.cgroups().map(|x| x.0),
+            ProcessTask::Process { proc: x, .. } => x.cgroups(),
             _ => Err(ProcError::Other("not supported".to_string())),
         }
     }
