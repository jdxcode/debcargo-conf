Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pwhash
Upstream-Contact: Ivan Nejgebauer <inejge@gmail.com>
Source: https://github.com/inejge/pwhash

Files: *
Copyright: 2016-2022 Ivan Nejgebauer <inejge@gmail.com>
License: MIT

Files: src/des_crypt.rs
Copyright:
 2016 Ivan Nejgebauer <inejge@gmail.com>
 2001 Iris Van den Broeke, Daniel Deville
 1996 Aki Yoshida
License: other
 Permission to use, copy, modify and distribute this software
 for non-commercial or commercial purposes and without fee is
 hereby granted provided that this copyright notice appears in
 all copies.

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Johannes Schauer Marin Rodrigues <josch@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
