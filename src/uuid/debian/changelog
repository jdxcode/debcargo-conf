rust-uuid (1.10.0-1) unstable; urgency=medium

  * Team upload.
  * Package uuid 1.10.0 from crates.io using debcargo 2.6.1
    + Closes: #1069669
  * Refresh patches
  * Remove disable-zerocopy patch, since we have zerocopy packaged
  * Remove remove-windows-sys patch, since upstream no longer depends on it

 -- James McCoy <jamessan@debian.org>  Thu, 25 Jul 2024 17:17:09 -0400

rust-uuid (1.6.1-1) unstable; urgency=medium

  * Team upload.
  * Package uuid 1.6.1 from crates.io using debcargo 2.6.1 (Closes: #1060873)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 16 Jan 2024 09:42:00 +0000

rust-uuid (1.4.1-1) unstable; urgency=medium

  * Team upload.
  * Package uuid 1.4.1 from crates.io using debcargo 2.6.0
  * Stop patching arbitary dependency, upstream no longer has it pinned.
  * Update patches for new upstream.
  * Remove optional (in the cargo sense) dependency on borsh as it is not
    in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 29 Jul 2023 03:13:49 +0000

rust-uuid (1.2.1-1) unstable; urgency=medium

  * Team upload.
  * Package uuid 1.2.1 from crates.io using debcargo 2.5.0
  * Remove specific version pin from arbitrary dependency, upstream pinned it
    for MSRV reasons, but we don't need that in Debian.
  * Remove dev-dependency on windows-sys.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 13 Nov 2022 21:23:35 +0000

rust-uuid (1.1.2-2) unstable; urgency=medium

  * Team upload.
  * Package uuid 1.1.2 from crates.io using debcargo 2.5.0
  * Drop now-empty md6-0.7.patch
  * Re-instate and update patch to use rustcrypto sha1 implementation.
  * Disable macro-diagnostics feature.
  * Disable zerocopy feature.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 14 Aug 2022 22:35:26 +0000

rust-uuid (1.1.2-1) unstable; urgency=medium

  * Package uuid 1.1.2 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 14 Aug 2022 18:49:36 +0200

rust-uuid (0.8.1-5) unstable; urgency=medium

  * Team upload.
  * Package uuid 0.8.1 from crates.io using debcargo 2.4.4
  * Port "v5" support to use sha1 0.10 (the rustcrypto version), upstream has
    moved to sha1-smol, but that is not in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 03 Jun 2022 17:51:59 +0000

rust-uuid (0.8.1-4) unstable; urgency=medium

  * Team upload.
  * Package uuid 0.8.1 from crates.io using debcargo 2.4.4
  * Bump rand dependency to 0.8 and disable features that depend on
    obsolete rand features.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 05 Feb 2022 13:11:43 +0000

rust-uuid (0.8.1-3) unstable; urgency=medium

  * Team upload.
  * Package uuid 0.8.1 from crates.io using debcargo 2.4.2

  [ Sylvestre Ledru ]
  * Package uuid 0.8.1 from crates.io using debcargo 2.4.0
  * Disable autopkgtest because uses some nightly features

 -- Peter Michael Green <plugwash@debian.org>  Sun, 12 Apr 2020 22:00:52 +0000

rust-uuid (0.8.1-2) unstable; urgency=medium

  * Team upload.
  * Package uuid 0.8.1 from crates.io using debcargo 2.4.0

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 29 Dec 2019 11:54:22 +0100

rust-uuid (0.8.1-1) unstable; urgency=medium

  * Team upload.
  * Package uuid 0.8.1 from crates.io using debcargo 2.2.10

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 29 Oct 2019 09:23:10 +0100

rust-uuid (0.7.4-1) unstable; urgency=medium

  * Package uuid 0.7.4 from crates.io using debcargo 2.2.10

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 14 Oct 2019 10:56:38 +0200

rust-uuid (0.7.2-2) unstable; urgency=medium

  * Package uuid 0.7.2 from crates.io using debcargo 2.2.10
  * Source upload for migration

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 06 Aug 2019 18:26:42 +0200

rust-uuid (0.7.2-1) unstable; urgency=medium

  * Package uuid 0.7.2 from crates.io using debcargo 2.2.10

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 03 Feb 2019 21:26:18 +0100

rust-uuid (0.7.1-1) unstable; urgency=medium

  * Package uuid 0.7.1 from crates.io using debcargo 2.2.7

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 14 Oct 2018 12:13:13 +0200
