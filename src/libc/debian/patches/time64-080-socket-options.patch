This patch is based on the commit described below from the upstream time64
pull request, adapted for use in the Debian package by Peter Michael Green.

commit 17ad6e6394956e535887900ed8ff5949fa4c37fc
Author: Ola x Nilsson <olani@axis.com>
Date:   Mon Mar 20 14:21:04 2023 +0100

    linux: Set SO_TIMESTAMP* and SO_RCVTIMEO and SO_SNDTIMEO
    
    The actual values may be different on 32bit archs and glibc with
    64-bit time.

diff --git a/src/unix/linux_like/linux/arch/generic/mod.rs b/src/unix/linux_like/linux/arch/generic/mod.rs
index 2f437e16db..2e319f5823 100644
--- a/src/unix/linux_like/linux/arch/generic/mod.rs
+++ b/src/unix/linux_like/linux/arch/generic/mod.rs
@@ -37,10 +37,6 @@ pub const SO_PASSCRED: c_int = 16;
 pub const SO_PEERCRED: c_int = 17;
 pub const SO_RCVLOWAT: c_int = 18;
 pub const SO_SNDLOWAT: c_int = 19;
-pub const SO_RCVTIMEO: c_int = 20;
-pub const SO_SNDTIMEO: c_int = 21;
-// pub const SO_RCVTIMEO_OLD: c_int = 20;
-// pub const SO_SNDTIMEO_OLD: c_int = 21;
 pub const SO_SECURITY_AUTHENTICATION: c_int = 22;
 pub const SO_SECURITY_ENCRYPTION_TRANSPORT: c_int = 23;
 pub const SO_SECURITY_ENCRYPTION_NETWORK: c_int = 24;
@@ -49,18 +45,35 @@ pub const SO_ATTACH_FILTER: c_int = 26;
 pub const SO_DETACH_FILTER: c_int = 27;
 pub const SO_GET_FILTER: c_int = SO_ATTACH_FILTER;
 pub const SO_PEERNAME: c_int = 28;
-pub const SO_TIMESTAMP: c_int = 29;
+
+cfg_if! {
+    if #[cfg(all(gnu_time64_abi,
+                 any(target_arch = "arm", target_arch = "x86")))] {
+        pub const SO_TIMESTAMP: c_int = 63;
+        pub const SO_TIMESTAMPNS: c_int = 64;
+        pub const SO_TIMESTAMPING: c_int = 65;
+        pub const SO_RCVTIMEO: c_int = 66;
+        pub const SO_SNDTIMEO: c_int = 67;
+    } else {
+        pub const SO_TIMESTAMP: c_int = 29;
+        pub const SO_TIMESTAMPNS: c_int = 35;
+        pub const SO_TIMESTAMPING: c_int = 37;
+        pub const SO_RCVTIMEO: c_int = 20;
+        pub const SO_SNDTIMEO: c_int = 21;
+    }
+}
 // pub const SO_TIMESTAMP_OLD: c_int = 29;
+// pub const SO_TIMESTAMPNS_OLD: c_int = 35;
+// pub const SO_TIMESTAMPING_OLD: c_int = 37;
+// pub const SO_RCVTIMEO_OLD: c_int = 20;
+// pub const SO_SNDTIMEO_OLD: c_int = 21;
+
 pub const SO_ACCEPTCONN: c_int = 30;
 pub const SO_PEERSEC: c_int = 31;
 pub const SO_SNDBUFFORCE: c_int = 32;
 pub const SO_RCVBUFFORCE: c_int = 33;
 pub const SO_PASSSEC: c_int = 34;
-pub const SO_TIMESTAMPNS: c_int = 35;
-// pub const SO_TIMESTAMPNS_OLD: c_int = 35;
 pub const SO_MARK: c_int = 36;
-pub const SO_TIMESTAMPING: c_int = 37;
-// pub const SO_TIMESTAMPING_OLD: c_int = 37;
 pub const SO_PROTOCOL: c_int = 38;
 pub const SO_DOMAIN: c_int = 39;
 pub const SO_RXQ_OVFL: c_int = 40;
diff --git a/src/unix/linux_like/linux/arch/mips/mod.rs b/src/unix/linux_like/linux/arch/mips/mod.rs
index 7699677026..82e091f4e9 100644
--- a/src/unix/linux_like/linux/arch/mips/mod.rs
+++ b/src/unix/linux_like/linux/arch/mips/mod.rs
@@ -33,8 +33,15 @@ pub const SO_RCVLOWAT: c_int = 0x1004;
 // NOTE: These definitions are now being renamed with _OLD postfix,
 // but CI haven't support them yet.
 // Some related consts could be found in b32.rs and b64.rs
-pub const SO_SNDTIMEO: c_int = 0x1005;
-pub const SO_RCVTIMEO: c_int = 0x1006;
+cfg_if! {
+    if #[cfg(gnu_time64_abi)] {
+        pub const SO_SNDTIMEO: c_int = 67;
+        pub const SO_RCVTIMEO: c_int = 66;
+    } else {
+        pub const SO_SNDTIMEO: c_int = 0x1005;
+        pub const SO_RCVTIMEO: c_int = 0x1006;
+    }
+}
 // pub const SO_SNDTIMEO_OLD: c_int = 0x1005;
 // pub const SO_RCVTIMEO_OLD: c_int = 0x1006;
 pub const SO_ACCEPTCONN: c_int = 0x1009;
@@ -88,9 +95,17 @@ pub const SO_BINDTOIFINDEX: c_int = 62;
 // NOTE: These definitions are now being renamed with _OLD postfix,
 // but CI haven't support them yet.
 // Some related consts could be found in b32.rs and b64.rs
-pub const SO_TIMESTAMP: c_int = 29;
-pub const SO_TIMESTAMPNS: c_int = 35;
-pub const SO_TIMESTAMPING: c_int = 37;
+cfg_if! {
+    if #[cfg(gnu_time64_abi)] {
+        pub const SO_TIMESTAMP: c_int = 63;
+        pub const SO_TIMESTAMPNS: c_int = 64;
+        pub const SO_TIMESTAMPING: c_int = 65;
+    } else {
+        pub const SO_TIMESTAMP: c_int = 29;
+        pub const SO_TIMESTAMPNS: c_int = 35;
+        pub const SO_TIMESTAMPING: c_int = 37;
+    }
+}
 // pub const SO_TIMESTAMP_OLD: c_int = 29;
 // pub const SO_TIMESTAMPNS_OLD: c_int = 35;
 // pub const SO_TIMESTAMPING_OLD: c_int = 37;
diff --git a/src/unix/linux_like/linux/arch/powerpc/mod.rs b/src/unix/linux_like/linux/arch/powerpc/mod.rs
index 27834dbfea..039839995c 100644
--- a/src/unix/linux_like/linux/arch/powerpc/mod.rs
+++ b/src/unix/linux_like/linux/arch/powerpc/mod.rs
@@ -21,8 +21,15 @@ pub const SO_REUSEPORT: c_int = 15;
 // powerpc only differs in these
 pub const SO_RCVLOWAT: c_int = 16;
 pub const SO_SNDLOWAT: c_int = 17;
-pub const SO_RCVTIMEO: c_int = 18;
-pub const SO_SNDTIMEO: c_int = 19;
+cfg_if! {
+    if #[cfg(gnu_time64_abi)] {
+        pub const SO_SNDTIMEO: c_int = 67;
+        pub const SO_RCVTIMEO: c_int = 66;
+    } else {
+        pub const SO_SNDTIMEO: c_int = 19;
+        pub const SO_RCVTIMEO: c_int = 18;
+    }
+}
 // pub const SO_RCVTIMEO_OLD: c_int = 18;
 // pub const SO_SNDTIMEO_OLD: c_int = 19;
 pub const SO_PASSCRED: c_int = 20;
@@ -36,18 +43,26 @@ pub const SO_ATTACH_FILTER: c_int = 26;
 pub const SO_DETACH_FILTER: c_int = 27;
 pub const SO_GET_FILTER: c_int = SO_ATTACH_FILTER;
 pub const SO_PEERNAME: c_int = 28;
-pub const SO_TIMESTAMP: c_int = 29;
+cfg_if! {
+    if #[cfg(gnu_time64_abi)] {
+        pub const SO_TIMESTAMP: c_int = 63;
+        pub const SO_TIMESTAMPNS: c_int = 64;
+        pub const SO_TIMESTAMPING: c_int = 65;
+    } else {
+        pub const SO_TIMESTAMP: c_int = 29;
+        pub const SO_TIMESTAMPNS: c_int = 35;
+        pub const SO_TIMESTAMPING: c_int = 37;
+    }
+}
 // pub const SO_TIMESTAMP_OLD: c_int = 29;
+// pub const SO_TIMESTAMPNS_OLD: c_int = 35;
+// pub const SO_TIMESTAMPING_OLD: c_int = 37;
 pub const SO_ACCEPTCONN: c_int = 30;
 pub const SO_PEERSEC: c_int = 31;
 pub const SO_SNDBUFFORCE: c_int = 32;
 pub const SO_RCVBUFFORCE: c_int = 33;
 pub const SO_PASSSEC: c_int = 34;
-pub const SO_TIMESTAMPNS: c_int = 35;
-// pub const SO_TIMESTAMPNS_OLD: c_int = 35;
 pub const SO_MARK: c_int = 36;
-pub const SO_TIMESTAMPING: c_int = 37;
-// pub const SO_TIMESTAMPING_OLD: c_int = 37;
 pub const SO_PROTOCOL: c_int = 38;
 pub const SO_DOMAIN: c_int = 39;
 pub const SO_RXQ_OVFL: c_int = 40;
