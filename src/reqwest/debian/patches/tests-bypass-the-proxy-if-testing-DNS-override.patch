From 766cf3dcd895181e3342bf836cddec82ba7ab654 Mon Sep 17 00:00:00 2001
From: Simon Chopin <simon.chopin@canonical.com>
Date: Wed, 3 Jul 2024 14:07:55 +0200
Subject: [PATCH] tests: bypass the proxy if testing DNS override

If an explicit proxy is configured in the environment, then the request
will go through it rather than actually resolving the domain. Either
we're hitting the target domain on a weird port which will likely fail,
or the proxy straight up denies that weird request.

We're hitting this issue in the Ubuntu CI. Amazingly enough, the tests
actually passed *once* there, although the exact circumstances that
allowed this are still a bit of a mystery.

Bug-Ubuntu: https://bugs.launchpad.net/ubuntu/+source/rust-reqwest/+bug/2071789
Forwarded: https://github.com/seanmonstar/reqwest/pull/2341

---
 tests/client.rs | 4 ++++
 1 file changed, 4 insertions(+)

diff --git a/tests/client.rs b/tests/client.rs
index ce97456..18aaf4e 100644
--- a/tests/client.rs
+++ b/tests/client.rs
@@ -246,6 +246,7 @@ async fn overridden_dns_resolution_with_gai() {
         server.addr().port()
     );
     let client = reqwest::Client::builder()
+        .no_proxy()
         .resolve(overridden_domain, server.addr())
         .build()
         .expect("client builder");
@@ -270,6 +271,7 @@ async fn overridden_dns_resolution_with_gai_multiple() {
     // the server runs on IPv4 localhost, so provide both IPv4 and IPv6 and let the happy eyeballs
     // algorithm decide which address to use.
     let client = reqwest::Client::builder()
+        .no_proxy()
         .resolve_to_addrs(
             overridden_domain,
             &[
@@ -302,6 +304,7 @@ async fn overridden_dns_resolution_with_hickory_dns() {
         server.addr().port()
     );
     let client = reqwest::Client::builder()
+        .no_proxy()
         .resolve(overridden_domain, server.addr())
         .hickory_dns(true)
         .build()
@@ -328,6 +331,7 @@ async fn overridden_dns_resolution_with_hickory_dns_multiple() {
     // the server runs on IPv4 localhost, so provide both IPv4 and IPv6 and let the happy eyeballs
     // algorithm decide which address to use.
     let client = reqwest::Client::builder()
+        .no_proxy()
         .resolve_to_addrs(
             overridden_domain,
             &[
-- 
2.43.0

