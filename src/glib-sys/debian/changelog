rust-glib-sys (0.20.7-1) unstable; urgency=medium

  * Package glib-sys 0.20.7 from crates.io using debcargo 2.7.5

 -- Matthias Geiger <werdahias@debian.org>  Sun, 22 Dec 2024 20:43:35 +0100

rust-glib-sys (0.20.6-2) unstable; urgency=medium

  * Mark v2_84 feature as flaky

 -- Matthias Geiger <werdahias@debian.org>  Tue, 10 Dec 2024 23:05:17 +0100

rust-glib-sys (0.20.6-1) unstable; urgency=medium

  * Package glib-sys 0.20.6 from crates.io using debcargo 2.7.5

 -- Matthias Geiger <werdahias@debian.org>  Tue, 10 Dec 2024 11:04:47 +0100

rust-glib-sys (0.20.4-1) unstable; urgency=medium

  * Package glib-sys 0.20.4 from crates.io using debcargo 2.7.0

 -- Matthias Geiger <werdahias@debian.org>  Mon, 30 Sep 2024 16:35:05 +0200

rust-glib-sys (0.20.2-1) unstable; urgency=medium

  * Team upload.
  * Package glib-sys 0.20.2 from crates.io using debcargo 2.6.1

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 2 Sep 2024 09:40:26 -0400

rust-glib-sys (0.20.0-2) unstable; urgency=medium

  * Team upload.
  * Package glib-sys 0.20.0 from crates.io using debcargo 2.6.1
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 26 Aug 2024 11:37:58 -0400

rust-glib-sys (0.20.0-1) experimental; urgency=medium

  * Bump gir-rust-code-generator dependency to >= 0.20 
  * Package glib-sys 0.20.0 from crates.io using debcargo 2.6.1

 -- Matthias Geiger <werdahias@debian.org>  Sat, 10 Aug 2024 13:34:11 +0200

rust-glib-sys (0.19.5-2) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 04 May 2024 17:42:21 +0200

rust-glib-sys (0.19.5-1) experimental; urgency=medium

  * Package glib-sys 0.19.5 from crates.io using debcargo 2.6.1
  * Dropped patches, upstreamed
  * Build depend on gir-rust-code-generator >= 0.19.1, libglib2.0-dev >= 2.80
  * Version test-dependency on libglib2.0-dev

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 03 May 2024 16:59:59 +0200

rust-glib-sys (0.19.0-3) experimental; urgency=medium

  * Team upload
  * debian/patches/gir-type-size.patch: Replace the patch to remove
    problematic types instead.
  * debian/patches/add-missing-headers-definitions.patch: Replace the patch
    with the upstream variant.

 -- Zixing Liu <zixing.liu@canonical.com>  Wed, 17 Apr 2024 10:07:04 -0600

rust-glib-sys (0.19.0-2) experimental; urgency=medium

  * Team upload
  * Package glib-sys 0.19.0 from crates.io using debcargo 2.6.1

  [ Zixing Liu ]
  * debian/patches/add-missing-headers-definitions.patch: Add missing
    glib headers to the tests to fix build with glib 2.80 (LP: #2061202)
  * debian/patches/gir-type-size.patch: Add a patch to fix GIR type size
    comparison. (LP: #2061202)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 17 Apr 2024 08:40:23 -0400

rust-glib-sys (0.19.0-1) experimental; urgency=medium

  * Package glib-sys 0.19.0 from crates.io using debcargo 2.6.1
  * Updated copyright years and adjusted source url
  * Properly build-depend on packages needed for regeneration
  * Versioned gir-rust-code-generator dependency
  * Test-depend on libglib2.0-dev
  * Marked v2_80 feature as broken

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 07 Feb 2024 11:20:13 +0100

rust-glib-sys (0.18.1-2) unstable; urgency=medium

  * Team upload
  * Package glib-sys 0.18.1 from crates.io using debcargo 2.6.0
  * Drop obsolete MSRV patch
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 14:32:10 -0400

rust-glib-sys (0.18.1-1) experimental; urgency=medium

  * Package glib-sys 0.18.1 from crates.io using debcargo 2.6.0
  * Included patch for MSRV downgrade
  * Regenerate source code with debian tools before build (Closes:
    #1017905)

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 08 Sep 2023 20:28:42 +0200

rust-glib-sys (0.17.10-1) unstable; urgency=medium

  * Team upload.
  * Package glib-sys 0.17.10 from crates.io using debcargo 2.6.0
  * Removed inactive uploader, added my new mail address
  * Tests pass now; removed indication that they are broken

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 01 Aug 2023 23:48:21 +0200

rust-glib-sys (0.16.3-2) unstable; urgency=medium

  * Package glib-sys 0.16.3 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 20:31:34 +0200

rust-glib-sys (0.16.3-1) experimental; urgency=medium

  * Team upload.
  * Package glib-sys 0.16.3 from crates.io using debcargo 2.6.0
  * Added myself to uploaders
  * Added collapse_features
  * Dropped obsolete patches

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 15:24:21 +0200

rust-glib-sys (0.14.0-2) unstable; urgency=medium

  * debian/patches: Update to system-deps 6.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 05 Oct 2022 09:29:27 +0200

rust-glib-sys (0.14.0-1) unstable; urgency=medium

  * Package glib-sys 0.14.0 from crates.io using debcargo 2.4.4

  [ Sylvestre Ledru ]
  * Team upload.
  * Package glib-sys 0.10.1 from crates.io using debcargo 2.4.3

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 28 Nov 2021 23:11:47 +0100

rust-glib-sys (0.9.1-1) experimental; urgency=medium

  * Package glib-sys 0.9.1 from crates.io using debcargo 2.4.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Tue, 17 Dec 2019 10:27:06 +0100

rust-glib-sys (0.9.0-3) unstable; urgency=medium

  * Team upload.
  * Package glib-sys 0.9.0 from crates.io using debcargo 2.2.10
  * Source upload for migration

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 06 Aug 2019 18:23:21 +0200

rust-glib-sys (0.9.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with no changes.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 19 Jul 2019 11:34:22 -0300

rust-glib-sys (0.9.0-1) unstable; urgency=medium

  * Package glib-sys 0.9.0 from crates.io using debcargo 2.3.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Mon, 08 Jul 2019 05:10:00 +0200

rust-glib-sys (0.7.0-2) unstable; urgency=medium

  * Make libglib2.0-dev a run-time dependency

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Mon, 22 Oct 2018 14:11:07 +0200

rust-glib-sys (0.7.0-1) unstable; urgency=medium

  * Package glib-sys 0.7.0 from crates.io using debcargo 2.2.7

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Thu, 04 Oct 2018 22:59:49 +0200
