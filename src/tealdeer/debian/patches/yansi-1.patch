This patch is based on the commit described below, taken from upstream pull
request https://github.com/tealdeer-rs/tealdeer/pull/389

commit 859c97b90a2947bd16b010c5db01c37932bb21e8
Author: Blair Noctis <n@sail.ng>
Date:   Tue Nov 5 19:17:58 2024 +0800

    Upgrade yansi: 0.5.1 -> 1.0.1
    
    - Adapt to `thing.paint(style)` API; was `style.paint(thing)`
    - Remove `yansi::Paint::enable_windows_ascii()` in style usage decision;
      removed in yansi commit b186eb5bfb, which introduced "automatic"
      support for Windows: "If support is not available, styling is disabled
      and no styling sequences are emitted", fitting the `Auto` option

Index: tealdeer/src/config.rs
===================================================================
--- tealdeer.orig/src/config.rs
+++ tealdeer/src/config.rs
@@ -57,7 +57,7 @@ impl From<RawColor> for Color {
             RawColor::Cyan => Self::Cyan,
             RawColor::White => Self::White,
             RawColor::Ansi(num) => Self::Fixed(num),
-            RawColor::Rgb { r, g, b } => Self::RGB(r, g, b),
+            RawColor::Rgb { r, g, b } => Self::Rgb(r, g, b),
         }
     }
 }
Index: tealdeer/src/main.rs
===================================================================
--- tealdeer.orig/src/main.rs
+++ tealdeer/src/main.rs
@@ -249,20 +249,13 @@ fn main() {
     let args = Cli::parse();
 
     // Determine the usage of styles
-    #[cfg(target_os = "windows")]
-    let ansi_support = yansi::Paint::enable_windows_ascii();
-    #[cfg(not(target_os = "windows"))]
-    let ansi_support = true;
     let enable_styles = match args.color.unwrap_or_default() {
         // Attempt to use styling if instructed
         ColorOptions::Always => true,
         // Enable styling if:
-        // * There is `ansi_support`
         // * NO_COLOR env var isn't set: https://no-color.org/
         // * The output stream is stdout (not being piped)
-        ColorOptions::Auto => {
-            ansi_support && env::var_os("NO_COLOR").is_none() && io::stdout().is_terminal()
-        }
+        ColorOptions::Auto => env::var_os("NO_COLOR").is_none() && io::stdout().is_terminal(),
         // Disable styling
         ColorOptions::Never => false,
     };
Index: tealdeer/src/output.rs
===================================================================
--- tealdeer.orig/src/output.rs
+++ tealdeer/src/output.rs
@@ -3,6 +3,7 @@
 use std::io::{self, BufRead, Write};
 
 use anyhow::{Context, Result};
+use yansi::Paint;
 
 use crate::{
     cache::PageLookupResult,
@@ -86,11 +87,11 @@ fn print_snippet(
     use PageSnippet::*;
 
     match snip {
-        CommandName(s) => write!(writer, "{}", style.command_name.paint(s)),
-        Variable(s) => write!(writer, "{}", style.example_variable.paint(s)),
-        NormalCode(s) => write!(writer, "{}", style.example_code.paint(s)),
-        Description(s) => writeln!(writer, "  {}", style.description.paint(s)),
-        Text(s) => writeln!(writer, "  {}", style.example_text.paint(s)),
+        CommandName(s) => write!(writer, "{}", s.paint(style.command_name)),
+        Variable(s) => write!(writer, "{}", s.paint(style.example_variable)),
+        NormalCode(s) => write!(writer, "{}", s.paint(style.example_code)),
+        Description(s) => writeln!(writer, "  {}", s.paint(style.description)),
+        Text(s) => writeln!(writer, "  {}", s.paint(style.example_text)),
         Linebreak => writeln!(writer),
     }
 }
Index: tealdeer/src/utils.rs
===================================================================
--- tealdeer.orig/src/utils.rs
+++ tealdeer/src/utils.rs
@@ -1,4 +1,4 @@
-use yansi::Color;
+use yansi::{Color, Paint};
 
 /// Print a warning to stderr. If `enable_styles` is true, then a yellow
 /// message will be printed.
@@ -14,7 +14,7 @@ pub fn print_error(enable_styles: bool,
 
 fn print_msg(enable_styles: bool, message: &str, prefix: &'static str, color: Color) {
     if enable_styles {
-        eprintln!("{}{}", color.paint(prefix), color.paint(message));
+        eprintln!("{}{}", prefix.paint(color), message.paint(color));
     } else {
         eprintln!("{message}");
     }
Index: tealdeer/Cargo.toml
===================================================================
--- tealdeer.orig/Cargo.toml
+++ tealdeer/Cargo.toml
@@ -99,7 +99,7 @@ version = "0.8.19"
 version = "2.0.1"
 
 [dependencies.yansi]
-version = "0.5"
+version = "1"
 
 [dependencies.zip]
 version = "< 3"
