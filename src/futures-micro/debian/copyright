Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: futures-micro
Upstream-Contact: James Laver <james.laver@gmail.com>
Source: https://github.com/irrustible/futures-micro

Files: *
Copyright:
 2020-2021 James Laver <james.laver@gmail.com>
 2020 Matthieu le Brazidec
 2020 Stjepan Glavina
 2020 Erik Zscheile
License: APACHE-2-LLVM-EXCEPTIONS

Files: debian/*
Copyright:
 2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2023 Arnaud Ferraris <aferraris@debian.org>
License: APACHE-2-LLVM-EXCEPTIONS

License: APACHE-2-LLVM-EXCEPTIONS
 On Debian systems the full text of the Apache Software License 2.0 can be
 found in the `/usr/share/common-licenses/Apache-2.0' file.
 .
 ---- LLVM Exceptions to the Apache 2.0 License ----
 .
 As an exception, if, as a result of your compiling your source code, portions
 of this Software are embedded into an Object form of such source code, you
 may redistribute such embedded portions in such Object form without complying
 with the conditions of Sections 4(a), 4(b) and 4(d) of the License.
 .
 In addition, if you combine or link compiled forms of this Software with
 software that is licensed under the GPLv2 ("Combined Software") and if a
 court of competent jurisdiction determines that the patent provision (Section
 3), the indemnity provision (Section 9) or other Section of the License
 conflicts with the conditions of the GPLv2, you may retroactively and
 prospectively choose to deem waived or otherwise exclude such Section(s) of
 the License, but only in their entirety and only with respect to the Combined
 Software.
