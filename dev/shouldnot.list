# This list consists of one RFC822 stanza per entry, like d/control.
#
# Currently there are 3 fields: Crate, Version, Reason.
# - Crate: Name of the described crate.
# - Version: Applicable version(s), can be omitted if entire crate applies.
# - Reason: Why it's included, can span multiple lines.
#
# Stanzas are ordered by crate names, alphabetically.

Crate: aes-ctr
Reason: Deprecated upstream, use aes.

Crate: aes-soft
Reason: Deprecated upstream, use aes.

Crate: block-modes
Reason: Deprecated upstream, use https://github.com/RustCrypto/block-modes.

Crate: cargo-vendor
Reason: The functionality now already exists as part of cargo, so there is no need to package it.

Crate: criterion
Reason: Packaged by Jonas at https://salsa.debian.org/debian/rust-criterion

Crate: criterion-plot
Version: 0.5+
Reason: Packaged by Jonas at https://salsa.debian.org/debian/rust-criterion

Crate: gcc
Reason: Deprecated upstream, use cc.

Crate: miniz-sys
Reason: Please don't package this for the time being. flate2 was patched to not use miniz, and instead the system zlib. This seems to be working fine. Typically, crates depend on flate2 rather than miniz directly. If a crate depends on miniz directly, try persuading them to use flate2 instead. (If this really needs to be packaged, we should first package miniz as a C static library Debian package, and *then* package miniz-sys here.)

Crate: tempdir
Reason: Deprecated upstream, use tempfile.

Crate: rustfmt
Reason: Deprecated upstream.

Crate: synom
Reason: This is only needed for old versions of syn.

Crate: lazy-regex-proc-macros
Reason: Packaged by jonas in https://salsa.debian.org/debian/rust-lazy-regex

Crate: webpki-roots
Reason: duplicates ca-certificates, rdeps should use native-certs or similar crates/features to pick up ca-certificates contents instead (https://bugs.debian.org/1069946)
